//# cubicFUSION Picasa for jQuery
//# Version 1.01.00
//#
//# portalZINE(R) NMN
//# http://alex.portalzine.tv
//#
//# Alexander Graef
//# portalzine@gmail.com
//#
//# Copyright 2010-2013 portalZINE(R) NMN, Alexander Graef, Germany
//#
//# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//#
//# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
//# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
//# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
//* ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//#
//# If you use the cubicFUSION Picasa plugin for your commercial projects,  you need a valid license.
//#
//# Commercial redistribution of the code is not permitted except by arrangement with  portalZINE(R) NMN, Alexander Graef, Germany.
//*

(function ($) {
    $.cfPicasa= {        
		templateReplace : function(template, vars){
			return  template.replace(/{[^{}]+}/g, function(key){
    			return vars[key.replace(/[{}]+/g, "")] || "";
			});	
		},
		onEvent: function (image, settings) {      	
			
			// nothing here yet
			
        }
    };
    var methods = {
		
		init: function(options){
			 this.settings = {
               	userID			:'',
				albumID			:'',
				thumbCount 		: 30,
				thumbSize 		: '104', 	// cropped: 32, 48, 64, 72, 104, 144, 150, 160  / uncropped: 94, 110, 128, 200, 220, 288, 320, 400, 512, 576, 640, 720, 800, 912, 1024, 1152, 1280, 1440, 1600
				thumbCropped 	: 'c', 		// c cropped /  u uncropped,
				searchLimit     : '',
				template 		: "<li><a href='{gplusLink}' target='_BLANK'><img src='{thumb}'></a></li>"
			};
            if (options) {
                $.extend(this.settings, options);
            }
            
			var settings = this.settings;
           
			return this.each(function (a) {          
				var element		= $(this);
				var me = settings;				
				if(element.attr("data-userid")) 	me.userID 		= element.attr("data-userid");
				if(element.attr("data-albumid")) 	me.albumID 		= element.attr("data-albumid");
				if(element.attr("data-thumbcount")) me.thumbCount 	= element.attr("data-thumbcount");
				if(element.attr("data-thumbsize")) 	me.thumbSize 	= element.attr("data-thumbsize");
				if(element.attr("data-cropped")) 	me.thumbCropped = element.attr("data-cropped");
				if(element.attr("data-search")) 	me.searchLimit 	= element.attr("data-search");
				
				var dataLink 	= "http://picasaweb.google.com/data/feed/base/user/"+me.userID+"/albumid/"+me.albumID+"?kind=photo&thumbsize="+me.thumbSize+""+me.thumbCropped+"&access=public&alt=json";
				
				if(element.attr("data-search")  && me.searchLimit.length >0) dataLink = dataLink+'&q='+me.searchLimit;
			
				$.getJSON(dataLink, 
					function(data){						
						var picsCount = data.feed.entry.length - 1;
						for (var i = 0; i < me.thumbCount; i++) {
							
							var pic 		= data.feed.entry[i];
							var picasaurl	= pic.link[1].href;
							var thumb 		= pic.media$group.media$thumbnail[0].url;
							var id	 		= pic.id["$t"];
								id			= id.split("photoid");
								id			= id[1].replace(/\//g,"").split("?");
							var photoid 	= id[0];							
							var plusurl		= "https://plus.google.com/b/"+me.userID+"/photos/"+me.userID+"/albums/"+me.albumID+"/"+photoid
							var title   	= pic.media$group.media$title["$t"];
							var description =  pic.media$group.media$description["$t"];
							var image 		=  pic.media$group.media$content["url"];
							var template 	= $.cfPicasa.templateReplace(me.template,{'link': picasaurl, 'gplusLink':plusurl,'thumb': thumb,'description' : description,'title': title, 'image': image})
							
							element.append(template);							
						}
					});     		
				
			});
		}	
	}
	
	$.fn.extend({            
		cfPicasa: function (method) {		
			 
    		if ( methods[method] ) {
      			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
    		} else if ( typeof method === 'object' || ! method ) {
      			return methods.init.apply( this, arguments );
    		} else {
      			$.error( 'Method ' +  method + ' does not exist on jQuery.cfPicasa' );
    		}  
           
        }
    });
})(jQuery);