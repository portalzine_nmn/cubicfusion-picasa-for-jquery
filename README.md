
cubicFUSION Picasa for jQuery
=====================================
* * *

http://www.portalzine.de

About
-----

Simple way to add Picasa / Google+ images to your website.

Usage
-----

Documentation: http://www.portalzine.de/projects/picasa/


License
-----------

/LICENSE.txt.
